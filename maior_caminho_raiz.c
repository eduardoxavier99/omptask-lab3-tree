#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef struct st_list {
  struct st_node *element;
  struct st_list *next;
} list_node;

typedef struct st_node {
  long int id;
  long int parent;
  int key;
  list_node *children;
} node;

typedef struct st_queue {
  node **array;
  long int head;
  long int tail;
  long int n;
} queue;

queue* init_queue(long int n){
  queue *q = (queue *) malloc(sizeof(queue));
  q->head = 0;
  q->tail = 0;
  q->n = n;
  q->array = (node **) malloc(sizeof(node*) * n);
  return q;
}

void enqueue(queue *q, node *x){
  (q->array)[(q->tail)++] = x;
  if (q->tail == q->n)
    q->tail = 0;
}

node* dequeue(queue *q){
  if (q->head == q->tail)
    return NULL;
  node *ret = (q->array)[(q->head)++];
  if (q->head == q->n)
    q->head = 0;
  return ret;
}

void print_node(node* x){
  printf("node:%ld key:%d\n", x->id, x->key);
  printf("parent: %ld\nchildren:", x->parent);
  list_node *current = x->children;
  while(current){
    printf("%ld ", (current->element)->id);
    current = current->next;
  }
  printf("\n");
}

void print_map(node **map, long int n){
  for(long int i = 0; i < n; i++){
    if (map[i])
      print_node(map[i]);
  }
}

void add_child(node *parent, node *child){

  list_node *head = parent->children;
  list_node *new = (list_node *) malloc(sizeof(list_node));
  new->element = child;
  new->next = head;
  parent->children = new;
}

int main(){

/* Part 1: Input and building the tree */
  long int n;
  scanf("%ld", &n);

  if (!n) {
    printf("0\n");
    return 0;
  }

  // allocating a map to store the nodes
  node **map = (node**) malloc(sizeof(node) * n);

  // reading and creating the nodes
  int key;
  for(long int i = 0; i < n; i++){
    scanf("%d", &key);
    node *new = (node*) malloc(sizeof(node));
    new->id = i;
    new->key = key;
    new->parent = -1;
    new->children = NULL;
    // print_node(new);
    map[i] = new;                       // adding the new node to the map
  }

  // reading the children of each node
  long int id_child, id_parent;
  int n_child;
  for(long int i = 0; i < n; i++){
    scanf("%ld", &id_parent);
    node *current = map[id_parent];
    node *child = NULL;

    scanf("%d", &n_child);
    for(int j = 0; j < n_child; j++){
      scanf("%ld", &id_child);
      child = map[id_child];
      child->parent = id_parent;
      add_child(current, child);       // adding the child address in the parent
    }
  }

  // print_map(map, n);               // debug

  // finding the root
  node *root, *current;
  for(long int i = 0; i < n; i++){
    current = map[i];
    if (current && current->parent == -1) {
      root = current;
      break;
    }
  }
  // print_node(root);              // debug

/* Part 2: computing the longest path from the root */

  double t_start = omp_get_wtime();

  // path[i] <- the weight of the path from root to i
  long int *path = (long int *) malloc(sizeof(long int) * n);

  // a queue to add all nodes in a recursive manner
  queue *q = init_queue(n);

  // the path from the root to the root is a corner case, since its parent is -1
  path[root->id] = root->key;
  // add root's children to the queue
  list_node *root_children = root->children;
  while (root_children){
    enqueue(q, root_children->element);
    root_children = root_children->next;
  }

  // processing all nodes until the queue is empty
  while ((current = dequeue(q))) {

    // calculates the path[i]
    path[current->id] = path[current->parent] + current->key;

    // add all children of current to the queue
    list_node *current_children = current->children;
    while (current_children){
      enqueue(q, current_children->element);
      current_children = current_children->next;
    }
  }

  double t_end = omp_get_wtime();

/* Part 3: Output */

  long int longest_path = path[0];
  for (long int i = 1; i < n; i++){
    if (path[i] > longest_path)
      longest_path = path[i];
  }
  printf("%ld\n", longest_path);

  // printf("%lf\n", t_end - t_start);                 // COMMENT THIS!

  return 0;
} // main
